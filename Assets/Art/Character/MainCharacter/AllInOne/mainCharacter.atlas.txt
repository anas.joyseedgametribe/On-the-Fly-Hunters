mainCharacter.png
size:512,512
filter:Linear,Linear
pma:true
purple/body
bounds:214,325,86,98
rotate:90
purple/head
bounds:116,411,116,98
purple/head death
bounds:0,411,116,98
red/body
bounds:312,325,86,98
rotate:90
red/hand_left
bounds:410,377,34,34
red/hand_right
bounds:410,377,34,34
red/head
bounds:348,411,116,98
red/head death
bounds:232,411,116,98
red/left_leg
bounds:464,441,45,68
red/right_leg
bounds:464,377,38,64
yellow/body
bounds:410,291,86,98
rotate:90
yellow/head
bounds:98,313,116,98
yellow/head death
bounds:0,295,116,98
rotate:90
