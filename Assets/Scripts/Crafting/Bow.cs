﻿public class Bow : BaseWeapon
{
    public float power;
    public BowArrow arrowPrefabs;
    public override void UseItem()
    {
        var shootArrow = Instantiate(arrowPrefabs);
        shootArrow.ShootArrow(power, damage);
        base.UseItem();
    }
}
