﻿using Sirenix.OdinInspector;

public class BaseWeapon : PickAbleItem
{
    public int durability;
    public int damage;
    [ReadOnly]
    public int useAmount = 0;

    public override void UseItem()
    {
        base.UseItem();

        useAmount += 1;
        if(useAmount >= durability)
        {
            DestroyItem();
        }
    }
}
