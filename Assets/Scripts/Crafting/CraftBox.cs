﻿using System;
using UnityEngine;

public class CraftBox : MonoBehaviour
{
    public Func<bool> CraftingAction = null;
    public Action OnFinishCraftingRecipe;
    private CraftingRecipe craftingRecipe;

    public float Duration => craftingRecipe != null ? craftingRecipe.duration : 0;
    private float timer = 0;
    public bool IsTouched { get; protected set; } = false;
    public bool IsTaken { get; protected set; } = false;
    private bool haveMaterial = false;

    public ProgressBarView barView;

    public void Initialize(Func<bool> pickUpAction, Action onFinishPickUpAction, CraftingRecipe recipe)
    {
        if (IsTouched) return;

        CraftingAction += pickUpAction;
        OnFinishCraftingRecipe += onFinishPickUpAction;

        IsTouched = true;
        IsTaken = false;

        craftingRecipe = recipe;
        haveMaterial = HaveMaterialRequeired(craftingRecipe?.materialNeeded);

        timer = Duration;
        Debug.Log(haveMaterial);
        barView?.gameObject.SetActive(haveMaterial);
    }

    public void DeInitialize(Func<bool> pickUpAction, Action onFinishPickUpAction)
    {
        CraftingAction -= pickUpAction;
        OnFinishCraftingRecipe -= onFinishPickUpAction;
        
        IsTouched = false;
        craftingRecipe = null;

        barView?.gameObject.SetActive(IsTouched);
    }

    private void Update()
    {
        var isGathering = CraftingAction?.Invoke();
        var timeStamp = Time.deltaTime;
        if (isGathering.HasValue && isGathering.Value && !IsTaken && haveMaterial)
        {
            timer -= timeStamp;
            if (timer < 0)
            {
                IsTaken = true;
                OnFinishCraftingRecipe?.Invoke();
                haveMaterial = HaveMaterialRequeired(craftingRecipe.materialNeeded);
            }
        }
        barView?.SetBar(timer, maxValue: Duration);
    }

    private bool HaveMaterialRequeired(InventoryItem[] materials)
    {
        if (materials == null) return false;
        foreach (var material in materials)
        {
            var available = BlackBoard.GameState.GetInventoryAmount(material.name);
            Debug.Log(available + " " + material.amount);
            if (available < material.amount) return false;
        }
        return true;
    }
}