﻿public class PotionItem : PickAbleItem
{
    public float amountHeal;

    public override void UseItem()
    {
        base.UseItem();
        var playerHp = GetComponentInParent<Health>();
        playerHp.GetHealth(amountHeal);

        DestroyItem();
    }
}
