﻿using UnityEngine;

public class BowArrow : MonoBehaviour
{
    public int damage;
    public Rigidbody2D rigidbody;

    public void ShootArrow(float power, float damage)
    {
        rigidbody.AddForce(Vector2.right * power, ForceMode2D.Impulse);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var health = collision.GetComponent<Health>();
        health?.Damage(damage);
        Destroy(gameObject);
    }
}