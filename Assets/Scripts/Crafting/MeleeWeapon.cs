﻿using UnityEngine;

public class MeleeWeapon : BaseWeapon
{
    public Collider2D attackCollider;
    private bool isAttack;
    public override void UseItem()
    {
        attackCollider.enabled = true;
        isAttack = true;
        base.UseItem();
    }
}