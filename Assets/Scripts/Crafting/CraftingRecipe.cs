﻿using UnityEngine;

[CreateAssetMenu(menuName ="Database/Recipe")]
public class CraftingRecipe : ScriptableObject
{
    public string recipeName;
    public InventoryItem[] materialNeeded;
    public float duration;
    public PickAbleItem prefabs;
}