﻿using UnityEngine;

public class DepositBox : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var character = collision.GetComponent<Character>();
        if (character != null)
        {
            AddItemInInventory(character.currentItem as MaterialItem, character);
        }
    }

    public void AddItemInInventory(MaterialItem item, Character character)
    {
        if (item == null) return;
        BlackBoard.GameState.AddInventoryItem(item.materialName);
        character.ReleaseItem();
        Destroy(item.gameObject);
    }
}
