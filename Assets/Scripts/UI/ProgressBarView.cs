using UnityEngine;
using UnityEngine.UI;

public class ProgressBarView : MonoBehaviour
{
    [Header("Bindings")]
    public Image fillImage;

    public void SetBar(float currentValue, float minValue = 0f, float maxValue = 1f)
    {
        var percentage = MathHelper.Remap(currentValue, minValue, maxValue, 0f, 1f);
        fillImage.fillAmount = percentage;
    }

    public void Plus10Percent()
    {
        float newProgress = fillImage.fillAmount + 0.1f;
        newProgress = Mathf.Clamp(newProgress, 0f, 1f);
        SetBar(newProgress);
    }

    public void Minus10Percent()
    {
        float newProgress = fillImage.fillAmount - 0.1f;
        newProgress = Mathf.Clamp(newProgress, 0f, 1f);
        SetBar(newProgress);
    }
}
