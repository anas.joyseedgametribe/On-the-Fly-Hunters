﻿using UnityEngine;

public class PickAbleItem : MonoBehaviour
{
    public ItemType type;
    public bool IsTaken { get; private set; } = false;

    public void PickItem(Transform parent, bool isWeapon = false)
    {
        IsTaken = true;
        transform.SetParent(parent, false);
        transform.localPosition = Vector3.zero;
        if (isWeapon) transform.localEulerAngles = Vector3.forward * -90f;
    }

    public void ThrowItem(Transform parent)
    {
        IsTaken = false;
        transform.SetParent(null, false);

        transform.position = parent.position;
        transform.Translate(Vector3.down * 1f);
        transform.localEulerAngles = Vector3.zero;
    }

    protected void DestroyItem()
    {
        var character = GetComponentInParent<Character>();
        var currentItem = character.currentItem;
        character.ReleaseItem();
        Destroy(currentItem.gameObject);
    }

    public virtual void UseItem()
    {

    }
}
