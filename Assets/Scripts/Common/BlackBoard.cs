﻿using UnityEngine;

public class BlackBoard : Singleton<BlackBoard>
{
    [SerializeField]
    private GameState gameState;

    public static GameState GameState => Instance.gameState;
}
