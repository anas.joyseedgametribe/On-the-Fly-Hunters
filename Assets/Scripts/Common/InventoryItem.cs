﻿using Sirenix.OdinInspector;
using System.Collections;

[System.Serializable]
public class InventoryItem
{
    [ValueDropdown("GetAllMaterials", IsUniqueList = true)]
    public string name;
    public int amount;

    private IEnumerable GetAllMaterials()
    {
        return MaterialSettings.Instance.GetAllMaterialNames();
    }
}
