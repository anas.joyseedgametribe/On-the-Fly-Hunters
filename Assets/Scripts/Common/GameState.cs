﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameState : MonoBehaviour
{
    public List<InventoryItem> inventoryItem;

    public void AddInventoryItem(string name, int amount = 1)
    {
        var item = inventoryItem.FirstOrDefault(item => item.name == name);
        if(item != null)
        {
            item.amount += amount;
        }
        else
        {
            item = new InventoryItem
            {
                name = name,
                amount = amount
            };
            inventoryItem.Add(item);
        }
    }

    public int GetInventoryAmount(string name)
    {
        var item = inventoryItem.FirstOrDefault(item => item.name == name);
        return item != null ? item.amount : 0;
    }

    public bool UseInventoryItem(string name, int amount)
    {
        var item = inventoryItem.FirstOrDefault(item => item.name == name);
        if (item == null || item.amount < amount) return false;
        
        item.amount -= amount;
        return true;
    }
}
