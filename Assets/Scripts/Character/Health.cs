using Sirenix.OdinInspector;
using System.Collections;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    [Header("Bindings")]
    public ProgressBarView healthBar;
    public TextMeshProUGUI healthNumberText;

    [Header("Parameters")]
    public float initialHealth = 10;
    public float maximumHealth = 10;
    public bool indestructible = false;
    public bool invulnerable;
    public float invincibilityDuration;
    public bool destroyOnDeath;
    public float delayBeforeDestroy;

    [Header("Events")]
    public UnityEvent onHit;
    public UnityEvent onDeath;
    public UnityEvent onHealed;

    [Header("Properties")]
    public FloatReactiveProperty currentHealth = new FloatReactiveProperty(100f);

    private Character character;

    private void Awake()
    {
        Initialization();
    }

    private void Initialization()
    {
        character = GetComponent<Character>();
        ResetHealthToMaximum();
    }

    public void Damage(float damage, bool isCritical = false)
    {
        if (invulnerable) return;
        if (damage <= 0) return;
        if (currentHealth.Value <= 0 && initialHealth != 0) return;

        currentHealth.Value -= damage;
        onHit?.Invoke();

        if (currentHealth.Value < 0)
        {
            currentHealth.Value = 0;
        }

        // Being invisible after taking some damage
        if (invincibilityDuration > 0)
        {
            DamageDisabled();
            StartCoroutine(DamageEnabled(invincibilityDuration));
        }

        // Reset health if eternal
        if (indestructible)
        {
            ResetHealthToMaximum();
        }

        // Die if current health reach zero
        if (currentHealth.Value <= 0)
        {
            Kill();
        }

        UpdateHealthBar();
    }

    private void Kill()
    {
        onDeath?.Invoke();

        if (delayBeforeDestroy > 0f)
        {
            Invoke("DestroyObject", delayBeforeDestroy);
        }
        else
        {
            DestroyObject();
        }
    }

    private void DestroyObject()
    {
        if (destroyOnDeath)
        {
            gameObject.SetActive(false);
        }
    }

    [Button]
    public void GetHealth(float health)
    {
        currentHealth.Value = Mathf.Min(currentHealth.Value + health, maximumHealth);
        onHealed?.Invoke();
        UpdateHealthBar();
    }

    public void ResetHealthToMaximum()
    {
        currentHealth.Value = maximumHealth;
        UpdateHealthBar();
    }

    public void SetHealth(float newValue)
    {
        currentHealth.Value = newValue;
        UpdateHealthBar();
    }

    private void UpdateHealthBar()
    {
        if (healthBar == null) return;
        healthBar.SetBar(currentHealth.Value, 0f, maximumHealth);

        if (healthNumberText == null) return;
        healthNumberText.text = $"{currentHealth.Value}";
    }

    private void DamageDisabled()
    {
        invulnerable = true;
    }

    private void DamageEnabled()
    {
        invulnerable = false;
    }

    private IEnumerator DamageEnabled(float delay)
    {
        yield return new WaitForSeconds(delay);
        DamageEnabled();
    }
}