using System;
using UnityEngine;

public class Character : MonoBehaviour
{
    [Header("Character Setup")]
    public GameObject characterModel;
    public Transform characterTransform;
    public Transform handTransform;
    public Animator animator;

    public float moveSpeed = 5f;

    private new Rigidbody2D rigidbody;

    private Vector2 movement;

    [Header("Character Action")]
    public KeyCode equipKey;
    public KeyCode useItemKey;

    public Transform carryTransform;
    public PickAbleItem currentItem = null;
    public PickAbleItem tryPickItem = null;
    private bool isGathering = false;

    private bool isPickCooldown = false;
    private float timerCooldown;
    private float pickCooldown = 0.1f;

    public CraftingRecipe recipe;

    private void Awake()
    {
        PreInitialization();
    }

    protected virtual void PreInitialization()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        MovementInput();
        ThrowOrPickObject();
        UseCarryItem();
        ResetPickCooldown();

        UpdateAnimators();
    }

    private void UseCarryItem()
    {
        if (Input.GetKey(useItemKey) && currentItem != null)
        {
            currentItem.UseItem();
        }        
    }

    private void FixedUpdate()
    {
        Movement();
    }

    protected virtual void Movement()
    {
        rigidbody.MovePosition(rigidbody.position + movement * (moveSpeed * Time.fixedDeltaTime));
    }

    private void ResetPickCooldown()
    {
        if (!isPickCooldown) return;

        timerCooldown -= Time.deltaTime;
        if (timerCooldown > 0) return;

        isPickCooldown = false;
    }

    private void SetPickCooldown()
    {
        timerCooldown = pickCooldown;
        isPickCooldown = true;
    }

    private void ThrowOrPickObject()
    {
        if (!Input.GetKeyDown(equipKey)) return;
        
        if (currentItem != null && !isPickCooldown)
        {
            ReleaseItem();
        }

        if (currentItem == null && tryPickItem != null && !isPickCooldown)
        {
            PickItem(tryPickItem);
            tryPickItem = null;
        }
    }

    public void ReleaseItem()
    {
        currentItem.ThrowItem(carryTransform);
        currentItem = null;
        SetPickCooldown();
    }

    private void MovementInput()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
    }

    protected virtual void UpdateAnimators()
    {
        if (animator == null) return;
    }

    private bool HoldAction()
    {
        return Input.GetKey(equipKey);
    }

    public void GetMaterials(MaterialItem prefab)
    {
        var obj = Instantiate(prefab);
        PickItem(obj);
    }

    private void PickItem(PickAbleItem item)
    {
        var parent = item.type == ItemType.Weapon ? handTransform : carryTransform;
        item.PickItem(parent, item.type == ItemType.Weapon);
        currentItem = item;
        SetPickCooldown();
    }

    private void GetRecipeItem()
    {
        var obj = Instantiate(recipe.prefabs);
        PickItem(obj);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var gatheringSpot = collision.GetComponent<GatheringSpot>();
        if(gatheringSpot != null && currentItem == null)
        {
            gatheringSpot.Initialize(HoldAction, GetMaterials);
            isGathering = true;
        }

        var craftingSpot = collision.GetComponent<CraftBox>();
        if (craftingSpot != null && currentItem == null)
        {
            craftingSpot.Initialize(HoldAction, GetRecipeItem, recipe);
            isGathering = true;
        }

        var pickableItem = collision.GetComponent<PickAbleItem>();
        if (currentItem == null && !isGathering)
        {
            tryPickItem = pickableItem;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        var gatheringSpot = collision.GetComponent<GatheringSpot>();
        if (gatheringSpot != null)
        {
            gatheringSpot.DeInitialize(HoldAction, GetMaterials);
            isGathering = false;
        }

        var craftingSpot = collision.GetComponent<CraftBox>();
        if (craftingSpot != null)
        {
            craftingSpot.DeInitialize(HoldAction, GetRecipeItem);
            isGathering = false;
        }
    }
}
