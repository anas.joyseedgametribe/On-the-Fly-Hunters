﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using UnityEngine;

public class GatheringSpot : MonoBehaviour
{
    [ValueDropdown("GetAllMaterials", IsUniqueList = true)]
    public string materialName;
    public float Duration => MaterialSettings.Instance.GetDuration(materialName);
    public MaterialItem prefab;

    private float timer = 0;
    public bool IsTouched { get; protected set; } = false;
    public bool IsTaken { get; protected set; } = false;
    public Func<bool> GatherMaterial = null;
    public Action<MaterialItem> OnFinishGatherMaterial;

    public ProgressBarView barView;

    private IEnumerable GetAllMaterials()
    {
        return MaterialSettings.Instance.GetAllMaterialNames();
    }

    private void OnEnable()
    {
        timer = Duration;
    }

    public void Initialize(Func<bool> gatherMaterial, Action<MaterialItem> onFinishGatherMaterial)
    {
        prefab.materialName = materialName;
        GatherMaterial += gatherMaterial;
        OnFinishGatherMaterial += onFinishGatherMaterial;
        
        IsTouched = true;
        IsTaken = false;

        timer = Duration;
        barView.gameObject.SetActive(IsTouched);
    }

    public void DeInitialize(Func<bool> gatherMaterial, Action<MaterialItem> onFinishGatherMaterial)
    {
        GatherMaterial -= gatherMaterial;
        OnFinishGatherMaterial -= onFinishGatherMaterial;
        IsTouched = false;
        barView.gameObject.SetActive(IsTouched);
    }

    private void Update()
    {
        var isGathering = GatherMaterial?.Invoke();
        var timeStamp = Time.deltaTime;
        if (isGathering.HasValue && isGathering.Value && !IsTaken)
        {
            timer -= timeStamp;
            if(timer < 0)
            {
                IsTaken = true;
                OnFinishGatherMaterial?.Invoke(prefab);
            }
        }
        barView.SetBar(timer, maxValue: Duration);
    }
}
