﻿using Sirenix.Utilities;
using System.Collections.Generic;
using System.Linq;

[GlobalConfig("Assets/Resources/Configs")]
public class MaterialSettings : GlobalConfig<MaterialSettings>
{
    public List<MaterialConfig> materialConfig;
    
    public List<string> GetAllMaterialNames()
    {
        return materialConfig.Select(mat => mat.name).ToList();
    }

    public float GetDuration(string name)
    {
        return materialConfig.FirstOrDefault(mat => mat.name == name).duration;
    }
}
