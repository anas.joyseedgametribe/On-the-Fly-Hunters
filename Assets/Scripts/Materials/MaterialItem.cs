﻿using Sirenix.OdinInspector;
using System.Collections;

public class MaterialItem : PickAbleItem
{
    [ValueDropdown("GetAllMaterials", IsUniqueList = true)]
    public string materialName;

    private IEnumerable GetAllMaterials()
    {
        return MaterialSettings.Instance.GetAllMaterialNames();
    }
}