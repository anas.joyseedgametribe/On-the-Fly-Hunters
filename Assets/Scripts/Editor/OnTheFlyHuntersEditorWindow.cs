﻿using Sirenix.OdinInspector.Editor;
using UnityEditor;

public class OnTheFlyHuntersEditorWindow : OdinMenuEditorWindow
{
    [MenuItem("On The Fly Hunters/Settings")]
    private static void OpenWindow()
    {
        GetWindow<OnTheFlyHuntersEditorWindow>().Show();
    }

    protected override OdinMenuTree BuildMenuTree()
    {
        var tree = new OdinMenuTree();
        tree.Add("Material Settings", MaterialSettings.Instance);
        return tree;
    }
}